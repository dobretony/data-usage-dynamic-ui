package ro.datausage.communications;

/**
 * @author Ioan-Alexandru Achim
 */
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;

public class DataCollector{
	private static DataCollector instance = new DataCollector();
	private ServerRequest request_handler;
	private CredentialManager cred_manager;
	
	public static void TRACE_COLLECTOR_INITIALIZE() {
		instance.InitializeCollection();
	}
	
	public static void TRACE_COLLECTOR_REGISTER_APP_ID(String appid){
		instance.RegisterAppId(appid);
	}
	
	public static void TRACE_COLLECTOR_CLEANUP() {
		instance.CleanupCollection();
	}
	
	public static void COLLECT_TOUCH_EVENT(Double xpos, Double ypos, String appstate ) {
		instance.CollectTouchEvent(xpos, ypos, appstate);
	}
	
	public static void COLLECT_SCROLL_EVENT(Double xpos, Double ypos, String appstate) {
		instance.CollectScrollEvent(xpos, ypos, appstate);
	}
	
	public static void COLLECT_DRAG_EVENT(Double xpos, Double ypos, String appstate) {
		instance.CollectDragEvent(xpos, ypos, appstate);
	}
	
	public static void COLLECT_GENERAL_EVENT(HashMap<String, String> data){
		instance.CollectGeneralEvent(data);
	}
	
	public static void COLLECT_GENERAL_EVENT(ArrayList<String> keys, ArrayList<String> values){
		if (keys.size() != values.size()){
			return;
		}
		HashMap<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < keys.size(); ++i){
			map.put(keys.get(i), values.get(i));
		}
		instance.CollectGeneralEvent(map);
	}
	
	public static void COLLECT_GENERAL_EVENT(String[] keys, String[] values){
		if (keys.length != values.length){
			return;
		}
		HashMap<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < keys.length; ++i){
			map.put(keys[i], values[i]);
		}
		instance.CollectGeneralEvent(map);
	}

	public static void COLLECT_GENERAL_EVENT(ArrayList<String> keys, String[] values){
		COLLECT_GENERAL_EVENT((String[])keys.toArray(), values);
	}

	public static void COLLECT_GENERAL_EVENT(String[] keys, ArrayList<String> values){
		COLLECT_GENERAL_EVENT(keys, (String[])values.toArray());
	}

	public static void COLLECT_GENERAL_EVENT(String data){
		instance.CollectGeneralEvent(data);
	}
	
	private void InitializeCollection() {
		request_handler = new ServerRequest();
		request_handler.InitializeThreading();
		request_handler.SetWebrequestServer("http://tracingtest2-iachim.rhcloud.com/api/receive");
		cred_manager = new CredentialManager();
		cred_manager.registerStartTime();
		request_handler.SetCredentialManager(cred_manager);
	}
	
	private void RegisterAppId(String appId){
		cred_manager.registerAppId(appId);
	}
	
	private void CleanupCollection() {
		request_handler.CleanupThreading();
	}
	
	private void CollectTouchEvent(Double xpos, Double ypos, String appstate) {
		JSONArray request = RequestGenerator.GenerateTouchEventRequest();
		request_handler.AddRequestToList(request);
	}
	
	private void CollectScrollEvent(Double xpos, Double ypos, String appstate) {
		JSONArray request = RequestGenerator.GenerateScrollEventRequest();
		request_handler.AddRequestToList(request);
	}
	
	private void CollectDragEvent(Double xpos, Double ypos, String appstate) {
		JSONArray request = RequestGenerator.GenerateDragEventRequest();
		request_handler.AddRequestToList(request);
	}
	
	private void CollectGeneralEvent(HashMap<String, String> data){
		JSONArray request = RequestGenerator.GenerateGeneralHashmapRequest(data);
		request_handler.AddRequestToList(request);
	}
	
	private void CollectGeneralEvent(String data){
		JSONArray request = RequestGenerator.GenerateGeneralStringRequest(data);
		request_handler.AddRequestToList(request);
	}
}
