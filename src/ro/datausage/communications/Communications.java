package ro.datausage.communications;

import ro.datausage.service.DataUsageRepository;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Communications {

	
	public static boolean checkInternetAvailability()
	{
		Context mContext = DataUsageRepository.getInstance().getServiceContext();
		
		ConnectivityManager connMgr = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if(networkInfo != null && networkInfo.isConnected()){
			return true;
		}
		
		return false;
	}
	
	
	
	
	
}
