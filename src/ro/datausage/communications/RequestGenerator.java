package ro.datausage.communications;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * 
 * @author Ioan Achim Alexandru
 *
 */
public class RequestGenerator {
	//TODO: complete requests based on android supplied info
	public static JSONArray GenerateTouchEventRequest() {
		return new JSONArray();
	}
	
	public static JSONArray GenerateScrollEventRequest() {
		return new JSONArray();
	}
	
	public static JSONArray GenerateDragEventRequest() {
		return new JSONArray();
	}
	
	public static JSONArray GenerateGeneralHashmapRequest(HashMap<String, String> data){
		JSONArray retval = new JSONArray();
		Iterator it = data.entrySet().iterator();
		for (;it.hasNext();){
			JSONObject item = new JSONObject();
			Map.Entry pairs = (Map.Entry)it.next();
			
			try {
				item.put((String)pairs.getKey(), (String)pairs.getValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			retval.put(item);
		}
		return retval;
	}
	
	public static JSONArray GenerateGeneralStringRequest(String data){
		JSONTokener tokener = new JSONTokener(data);
		JSONObject root = null;
		try {
			root = new JSONObject(tokener);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		JSONArray retval = new JSONArray();
		retval.put(root);
		return retval;
	}
}
