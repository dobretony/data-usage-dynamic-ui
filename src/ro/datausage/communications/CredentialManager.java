package ro.datausage.communications;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * 
 * @author Ioan Achim Alexandru
 *
 */
public class CredentialManager {
	private String app_id;
	private long app_time;
	private String user_email;
	private String user_id;
	private String password_id;
	
	
	public CredentialManager() {
		app_id = "";
	}
	
	public String getAppId() {
		return app_id;
	}
	
	public long getAppTime(){
		return app_time;
	}
	
	public void registerAppId(String appid){
		app_id = appid;
	}
	
	public void regiesterUserId(String userid)
	{
		user_id = userid;
	}
	
	public void registerUserEmail(String userEmail)
	{
		user_email = userEmail;
		
	}
	
	public void registerUserPassword(String passwordInClear)
	{
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			
			md.update(passwordInClear.getBytes());
			
			byte[] mdbytes = md.digest();
			
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < mdbytes.length; i++) {
		          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		    }
			
			password_id = sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		
		
		
	}
	
	public void registerStartTime(){
		Date d = new Date();
		app_time = d.getTime();
	}
}
