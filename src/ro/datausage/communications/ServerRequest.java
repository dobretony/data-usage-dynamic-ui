package ro.datausage.communications;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.datausage.constants.Constants;
import ro.datausage.service.DataUsageRepository;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Ioan Achim Alexandru
 *
 */
public class ServerRequest implements Runnable {
	private String server_address;
	private ArrayList<JSONArray> request_list = new ArrayList<JSONArray>();

	private int request_idx = 0;
	private int running_minimum_idx = -1;
	private int running_maximum_idx = -1;

	private CredentialManager credential_manager;

	private Object request_list_lock;

	private AtomicBoolean thread_initialized;

	public void InitializeThreading() {
		thread_initialized = new AtomicBoolean();
		thread_initialized.set(false);
		request_list_lock = new Object();
	}

	public void CleanupThreading() {
		//TODO: signal thread stop
		thread_initialized = null;
		request_idx = 0;
	}

	public void SetWebrequestServer(String server_address){
		this.server_address = server_address;
	}

	public void SetCredentialManager(CredentialManager manager) {
		credential_manager = manager;
	}

	private void _TryStartThread(){
		if (thread_initialized.get()) {
			return;
		}

		thread_initialized.set(true);
		(new Thread(this)).start();
	}

	public void run() {

		int min_idx = -1;
		int max_idx = -1;
		ArrayList<JSONArray> tls_request_list = new ArrayList<JSONArray>();
		synchronized (request_list_lock) {
			for (JSONArray str : request_list) {
				tls_request_list.add(str);
			}

			min_idx = running_minimum_idx;
			max_idx = running_maximum_idx;

			request_list.clear();
		}

		boolean sent = SendWebRequest(tls_request_list, min_idx, max_idx);
		if(sent == true)
		{
			Log.d("Test","S-a trimis cu success.");
		}else{
			Log.d("Test","Nope, Chuck Testa.");
		}

		if (null != thread_initialized){
			thread_initialized.set(false);
		}
	}

	public void AddRequestToList(JSONArray request) {
		synchronized (request_list_lock) {
			request_list.add(request);
			if (-1 == running_minimum_idx){
				running_minimum_idx = request_idx;
			}
			running_maximum_idx = request_idx;

			++request_idx;
		}

	}

	public void flush()
	{
		this._TryStartThread();
	}

	public boolean SendWebRequest(ArrayList<JSONArray> requests, int min_idx, int max_idx) {
		boolean bRet = false;

		HttpURLConnection connection = null;

		try {
			URL serverAddress = new URL(server_address);
			Log.d("Test", "incearca sa se conecteze la adresa");
			connection = (HttpURLConnection)serverAddress.openConnection();
			Log.d("Test"," a incercat sa deschida conexiunea la adresa.");
			_InitializeConnectionProperties(connection);
			_WriteBuffersToRequest(connection, requests, min_idx, max_idx);

			connection.connect();
			Log.d("Test", "S-a conectat la server si a incercat sa trimita datele.");
			bRet = _CheckRequestResponse(connection);
			Log.d("Test","Raspunsul returnat este : " + bRet);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
			connection = null;
		}

		return bRet;
	}

	private void _InitializeConnectionProperties(HttpURLConnection connection) throws IOException {

		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod(Constants.Strings.REQUEST_METHOD_TYPE);
		connection.setUseCaches(Constants.Booleans.REQUEST_USE_CACHING);
		connection.setReadTimeout(Constants.Integers.REQUEST_READ_TIMEOUT);
		connection.setConnectTimeout(Constants.Integers.REQUEST_CONNECT_TIMEOUT);
		connection.setRequestProperty("Content-type", "application/json");
	}

	private void _WriteBuffersToRequest(HttpURLConnection connection, ArrayList<JSONArray> requests, int min_idx, int max_idx) throws IOException, JSONException {
		OutputStream out = connection.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));

		JSONObject requestData = new JSONObject();
		_AttachCredentials(requestData);
		_AttachAdditionalData(requestData, min_idx, max_idx);
		_AttachRequests(requestData, requests);

		Log.d("Test", requestData.toString(1));

		writer.write(requestData.toString());

		writer.close();
		out.close();
	}

	private void _AttachCredentials(JSONObject requestData) throws JSONException {
		JSONObject credentialObject = new JSONObject();
		credentialObject.put("app_id", credential_manager.getAppId());

		requestData.put("credentials", credentialObject);
	}

	private void _AttachRequests(JSONObject requestData, ArrayList<JSONArray> requests) throws JSONException {
		JSONObject requestObject = new JSONObject();
		JSONArray requestList = new JSONArray();

		for (JSONArray req : requests) {
			requestList.put(req);
		}

		requestObject.put("req_count", requests.size());
		requestObject.put("req_list", requestList);

		requestData.put("requests", requestObject);
	}

	private void _AttachAdditionalData(JSONObject requestData, int min_idx, int max_idx) throws JSONException{
		JSONObject additionalObject = new JSONObject();
		additionalObject.put("language", Locale.getDefault().getDisplayName());
		additionalObject.put("minimum_index", min_idx);
		additionalObject.put("maximum_index", max_idx);
		additionalObject.put("app_start", credential_manager.getAppTime());

		requestData.put("additional", additionalObject);
	}

	private boolean _CheckRequestResponse(HttpURLConnection connection) throws IOException {
		boolean bRet = true;

		String res = connection.getResponseMessage();
		if(res != null){
			if (!res.equals("OK")) {
				bRet = false;
			}
		}
		else{
			Log.d("Test", "res este null.");
			return false;
		}
		return bRet;
	}
}
