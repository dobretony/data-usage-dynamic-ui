package ro.datausage.test;

import ro.datausage.constants.Constants;
import ro.datausage.exceptions.UndefinedDataUsagePropertyException;
import ro.datausage.service.DataTracker;
import ro.datausage.service.TrackingManagerProperties;
import ro.datausage.trackers.ActivityTracker;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnTouchListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		TrackingManagerProperties trackerProperties = new TrackingManagerProperties();
		trackerProperties.setID(Constants.Strings.TEST_APP_ID);
		trackerProperties.setContext(this);
		trackerProperties.setName("TestApp");
		trackerProperties.setServerIP("");
		trackerProperties.setServerPort("");
		
		try {
			DataTracker.initialize(trackerProperties);
		} catch (UndefinedDataUsagePropertyException e) {
			e.printStackTrace();
		}
		
		DataTracker.trackActivity(1, this.getClass().getName(), ActivityTracker.ACTIVITY_CREATE, "My activity was created.");
		
		Button bTest = (Button)findViewById(R.id.bTest);
		
		bTest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				DataTracker.trackOnButtonPressedEvent(R.id.bTest, "bTest");
				
			}
		});
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
		DataTracker.trackActivity(1, "MainActivity", ActivityTracker.ACTIVITY_DESTROY, "My activity was destroyed");
		DataTracker.terminate();
		
	}
	
	@Override
	protected void onStart() {

		DataTracker.trackActivity(1, "MainActivity", ActivityTracker.ACTIVITY_START, "Main activity has started.");
		super.onStart();
	}
	
	@Override
	protected void onResume() {

		DataTracker.trackActivity(1, this.getClass().getName(), ActivityTracker.ACTIVITY_RESUME, "Main Activity has resumed.");
		super.onResume();
	}
	
	@Override
	protected void onPause() {

		
		DataTracker.trackActivity(1, this.getClass().getName(), ActivityTracker.ACTIVITY_PAUSE, "Main Activity has paused");
		super.onPause();
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {

		
		
		
		return false;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		
		
		DataTracker.trackTouchEvent(10, "MainTouchEvents", ev.getX(), ev.getY());
		
		return super.dispatchTouchEvent(ev);
	}
	
}
