package ro.datausage.constants;

public class Constants {

	public static class Booleans {

		public static boolean ONLINE_CONNECTION = false;
		public static boolean IS_USER_AUTHENTICATED = false;
		public static boolean IS_BOUND_TO_SERVICE = false;
		public final static boolean REQUEST_USE_CACHING = false;
		
	}
	
	public static class Strings {

		public static final String TEST_APP_ID = "98CD4C36-DBAC-4F18-ACB0-707E7BC10BB1";
		public static final String TEST_USER_EMAIL = "dobre.tony@yahoo.com";
		public static final String TEST_USER_ID = "98CD4C36-DBAC-4F18-ACB0-707E7BC10BB1";
		public static final String TEST_USER_PASSWORD = "cfcd208495d565ef66e7dff9f98764da";
		public static final String REQUEST_METHOD_TYPE = "POST";
		public static final String DATABASE_NAME = "ro.datausage.db";
		public static final String TABLE_APP_INSTANCE_NAME = "appinstance";
		public static final String TABLE_APP_EVENTS_NAME = "appevents";
		
	}

	public static class Integers {
		
		public final static int THREAD_RUNNING_TIME = 1000;
		public final static int REQUEST_READ_TIMEOUT = 10000;
		public final static int REQUEST_CONNECT_TIMEOUT = 15000;
	
		public final static int DATABASE_VERSION = 1;
		
		// --------------------------------------------------------------------
		// Here we start to define the types of information we stock on the server
		// --------------------------------------------------------------------
		public final static int TYPE_BUTTON_INFO = 377340;//used mainly for button presses
		public final static int TYPE_ACTIVITY_INFO = 670200;
		public final static int TYPE_GENERAL_CLICK = 456354;
		public final static int TYPE_VIEW_INFO = 58419;
		public final static int TYPE_VIEW_CLICKED = 391989;
		public final static int TYPE_VIEW_HIGHLIGHT = 446154;
		public final static int TYPE_VIEW_HIDDEN = 390358;
		public final static int TYPE_VIEW_VISIBLE = 445246;
		public final static int TYPE_VIEW_INVISIBLE = 561339;
		public final static int TYPE_CHECKBOX_SELECTED = 861621;
		public final static int TYPE_CHECKBOX_DESELECTED = 94777;
		public final static int TYPE_EDIT_TEXT_INFO = 507931;
		public final static int TYPE_RADIO_BUTTON_INFO = 807241;
		public final static int TYPE_LIST_ITEM_INFO = 668323;
		public final static int TYPE_LIST_SELECTED = 45640;
		public final static int TYPE_IMAGE_VIEW_INFO = 116415;
		public final static int TYPE_SPINNER_PRESSED = 966864;
		public final static int TYPE_SPINNER_INFO = 555900;
		public final static int TYPE_ALERT_DIALOG_INFO = 174445;
		public final static int TYPE_HARDWARE_ON_BACK_INFO = 880799;
		public final static int TYPE_HOME_BUTTON_INFO  = 882699;
		public final static int TYPE_SETTINGS_MENU_INFO = 322545;
		public final static int TYPE_VOLUME_INFO = 651685;
		public final static int TYPE_FRAGMENT_INFO = 177007;
		public final static int TYPE_VARIABLE_INFO = 236603;
		public final static int TYPE_METHOD_INFO = 337234;
		public final static int TYPE_EXCEPTION_INFO = 486969;
		public final static int TYPE_ERROR_INFO = 486853;
		public final static int TYPE_INTENT_INFO = 597414;
		public final static int TYPE_GENERAL = 959806;
		
		
		
		// ---------------------------------------------------------------------
		//  TYPE for activity state
		// ---------------------------------------------------------------------
		public final static int TYPE_ACTIVITY_CREATE = 3;
		public final static int TYPE_ACTIVITY_START = 4;
		public final static int TYPE_ACTIVITY_RESUME = 5;
		public final static int TYPE_ACTIVITY_PAUSE = 6;
		public final static int TYPE_ACTIVITY_STOP = 7;
		public final static int TYPE_ACTIVITY_DESTROY = 8;
		
		
	}
	
	
}
