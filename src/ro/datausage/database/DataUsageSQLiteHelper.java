package ro.datausage.database;

import ro.datausage.constants.Constants;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DataUsageSQLiteHelper extends SQLiteOpenHelper {

	private static final String DATABASE_CREATE = "create table " + Constants.Strings.TABLE_APP_INSTANCE_NAME +
			"(" + "_id integer primary key autoincrement, " +
			"timestamp_begin text not null, " + "timestamp_end text, " + "app_version text not null, " + "app_name text not null);";
	private static final String DATABASE_CREATE_2 = "create table " + Constants.Strings.TABLE_APP_EVENTS_NAME + "(" +
			"_id integer primary key autoincrement, " + "data text not null, " + 
			"app_instance_id integer not null, " + "foreign key(app_instance_id) references " + Constants.Strings.TABLE_APP_INSTANCE_NAME + "(_id)" +");";
	
	
	
	public DataUsageSQLiteHelper(Context context)
	{
		super(context, Constants.Strings.DATABASE_NAME, null, Constants.Integers.DATABASE_VERSION);
		
	}
	
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		
		database.execSQL(DATABASE_CREATE);
		database.execSQL(DATABASE_CREATE_2);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		
		database.execSQL("DROP TABLE IF EXISTS " + Constants.Strings.TABLE_APP_EVENTS_NAME + ";");
		database.execSQL("DROP TABLE IF EXISTS " + Constants.Strings.TABLE_APP_INSTANCE_NAME + ";");
		onCreate(database);

	}

}