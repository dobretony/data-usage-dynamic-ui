package ro.datausage.database;

import org.json.JSONArray;
import org.json.JSONException;

import ro.datausage.communications.ServerRequest;
import ro.datausage.constants.Constants;
import ro.datausage.service.DataUsageRepository;
import ro.datausage.trackers.AppTracker;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class DataBaseAsyncTask extends AsyncTask<Void, Void, Void> {

	private DataUsageSQLiteHelper helper;
	private String[] app_InstanceColumns = {"_id", "timestamp_begin", "timestamp_end", "app_version", "app_name"};
	private String[] app_eventsColumns = {"_id", "data", "app_instance_id"};


	public DataBaseAsyncTask(DataUsageSQLiteHelper helper)
	{
		this.helper = helper;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... arg0) {

		synchronized (helper) {

			try{

				SQLiteDatabase database = helper.getWritableDatabase();


				Cursor cursor = database.query(Constants.Strings.TABLE_APP_INSTANCE_NAME, app_InstanceColumns, "timestamp_end not like \'end\'", null, null, null, null);

				ServerRequest serverRequest = DataUsageRepository.getInstance().mServerRequest;

				if(cursor.getCount() == 0) return null;

				cursor.moveToFirst();
				while(!cursor.isAfterLast())
				{

					long id = cursor.getLong(0);
					String timestampBegin = cursor.getString(1);
					String timestampEnd = cursor.getString(2);
					String appVersion = cursor.getString(3);
					String appName = cursor.getString(4);

					Log.d("test","Exista instanta: " + id + " begin:" + timestampBegin + ", end:" + timestampEnd + ", version:" + appVersion + ", appName:" + appName);


					//create the AppTracker
					AppTracker appTracker = new AppTracker();


					appTracker.setAppId(DataUsageRepository.getInstance().getAppID());
					appTracker.setAppInstanceId((int)id);
					appTracker.setAppName(appName);
					appTracker.setAppVersion(Integer.parseInt(appVersion));
					appTracker.setBeginTimestamp(timestampBegin);
					appTracker.setEndTimestamp(timestampEnd);
					appTracker.setDeviceID("");//if anonymous mode is on then the device id is null


					Cursor eventCursor = database.query(Constants.Strings.TABLE_APP_EVENTS_NAME, app_eventsColumns, "app_instance_id="+id, null, null, null, null);

					eventCursor.moveToFirst();
					while(!eventCursor.isAfterLast())
					{

						String data = eventCursor.getString(1);

						try {

							JSONArray dataArray = new JSONArray(data);
							appTracker.addTracker(dataArray);

						} catch (JSONException e) {
							e.printStackTrace();
						}

						eventCursor.moveToNext();
					}

					eventCursor.close();

					serverRequest.AddRequestToList(appTracker.toJSONArray());

					//dupa ce am adaugat instanta la server request
					database.delete(Constants.Strings.TABLE_APP_EVENTS_NAME, app_eventsColumns[2] + "=" + id, null);
					database.delete(Constants.Strings.TABLE_APP_INSTANCE_NAME, "_id="+id, null);

					cursor.moveToNext();
				}

				cursor.close();
				serverRequest.flush();

			}catch(SQLException e){

				Log.d("test","SQLEXception occured in DataBaseAsyncTask.doInBackGround: " + e.getMessage());

				this.cancel(true);

			}finally{
				helper.close();
			}

		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}


	@Override
	protected void onCancelled() {
		super.onCancelled();


	}
}
