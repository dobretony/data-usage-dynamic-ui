package ro.datausage.trackers;

import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * General application tracker class.
 * This class should not be used in the notification system, it is just a wrapper in order to 
 * easily create a JSON Array with the app instance that was launched.
 * @author Tony
 *
 */
public class AppTracker{

	/**
	 * All the JSONArray's of every event that happened in an app.
	 */
	private LinkedList<JSONArray> data;
	private int app_instance_id;
	private String begin_timestamp;
	private String end_timestamp;
	private String app_Name;
	private String app_id;
	private int app_version;
	private String deviceID;
	
	
	public AppTracker()
	{
		data = new LinkedList<JSONArray>();
	}

	/**
	 * Adds the JSONArray of a tracker to this application instance in order to send it to the server. 
	 * @param tracker
	 */
	public void addTracker(JSONArray tracker)
	{
		
		data.add(tracker);
		
	}
	
	/**
	 * Adds a tracker to this application instance.
	 * @param tracker
	 */
	public void addTracker(Tracker tracker)
	{
		try {
			
			data.add(tracker.getJSONRequest());
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * The general format for the application tracker is an array of:
	 * 1 - Application instance identification values. 
	 * 				( a JSONObject with the fields : app_id, begin_timestamp, end_timestamp, app_name, 
	 * 												app_version, app_instance_id. )
	 * 2 - Payload ( a JSONArray of the traces that you got from the application ( JSON Arrays) ) .
	 * @return
	 */
	@Override
	public String toString()
	{
		JSONArray retVal = new JSONArray();
		JSONObject object = new JSONObject();
		
		try {
			
			object.put("app_id", app_id);
			object.put("begin_timestamp", begin_timestamp);
			object.put("end_timestamp", end_timestamp);
			object.put("app_name", app_Name);
			object.put("app_version", app_version);
			object.put("app_instance_id", app_instance_id);
			
			JSONArray payload = new JSONArray();
			
			for(JSONArray tracker : this.data){
				
				payload.put(tracker);
				
			}
			
			retVal.put(object);
			retVal.put(payload);
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		
		return object.toString();
	}
	
	
	/**
	 * The general format for the application tracker is an array of:
	 * 1 - Application instance identification values. 
	 * 				( a JSONObject with the fields : app_id, begin_timestamp, end_timestamp, app_name, 
	 * 												app_version, app_instance_id. )
	 * 2 - Payload ( a JSONArray of the traces that you got from the application ( JSON Arrays) ) .
	 * @return
	 */
	public JSONArray toJSONArray()
	{
		JSONArray retVal = new JSONArray();
		
		JSONObject object = new JSONObject();
		
		try {
			
			object.put("app_id", app_id);
			object.put("begin_timestamp", begin_timestamp);
			object.put("end_timestamp", end_timestamp);
			object.put("app_name", app_Name);
			object.put("app_version", app_version);
			object.put("app_instance_id", app_instance_id);
			
			JSONArray payload = new JSONArray();
			
			for(JSONArray tracker : this.data){
				
				payload.put(tracker);
				
			}
			
			retVal.put(object);
			retVal.put(payload);
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return retVal;
		
	}
	

	public int getApp_instance_id() {
		return app_instance_id;
	}

	public void setAppInstanceId(int app_instance_id) {
		this.app_instance_id = app_instance_id;
	}

	public String getBeginTimestamp() {
		return begin_timestamp;
	}

	public void setBeginTimestamp(String begin_timestamp) {
		this.begin_timestamp = begin_timestamp;
	}

	public String getEndTimestamp() {
		return end_timestamp;
	}

	public void setEndTimestamp(String end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public String getAppName() {
		return app_Name;
	}

	public void setAppName(String app_Name) {
		this.app_Name = app_Name;
	}

	public int getAppVersion() {
		return app_version;
	}

	public void setAppVersion(int app_version) {
		this.app_version = app_version;
	}

	public String getAppId() {
		return app_id;
	}

	public void setAppId(String app_id) {
		this.app_id = app_id;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

}
