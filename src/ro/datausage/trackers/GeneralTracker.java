package ro.datausage.trackers;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.datausage.constants.Constants;
import ro.datausage.utils.Timestamp;

public class GeneralTracker extends Tracker {


	private Timestamp timestamp;
	private String payload;
	private HashMap<Timestamp, String> others;


	public GeneralTracker()
	{
		this.others = null;
	}


	public GeneralTracker(int id, String name, String message)
	{

		this.ID = id;
		this.name = name;
		this.payload = message;
		this.timestamp = new Timestamp();
		this.others = null;
	}



	@Override
	public String getName() {	
		return this.name;
	}

	@Override
	public int getID() {
		return this.ID;
	}

	@Override
	public void updateTracker(Tracker newTracker) {

		GeneralTracker gTracker = (GeneralTracker)newTracker;

		if(this.others == null){

			this.others = new HashMap<Timestamp, String>();
			this.others.put(this.timestamp, this.payload);

		}

		this.others.put(gTracker.timestamp, gTracker.payload);

	}

	@Override
	public boolean equals(Tracker tracker) {

		if(tracker.getClass().getName().equals(this.getClass().getName())){

			if(this.name != null && this.ID != -1 && tracker.getName() != null && tracker.getID() != -1)
				if(this.name.equals(tracker.name) && this.ID == tracker.getID())
					return true;
				else return false;
			else if(this.name != null && tracker.name != null)
				if(this.name.equals(tracker.name))
					return true;
				else
					return false;
			else if(this.ID != -1 && tracker.ID != -1)
				if(this.ID == tracker.getID())
					return true;
				else
					return false;
			else
				return false;	
		}

		return false;
	}


	/**
	 * The general structure for getJSONRequest of the genreal tracker is:
	 * 
	 * 1 - IDENTIFICATION INFO
	 * 			type:"TYPE_GENERAL"
	 * 			ID:""
	 * 			name:""
	 * 2 - payload a JSONArray with JSONObjects: 
	 * 			{
	 * 				timestamp:""
	 * 				payload:""
	 * 			}
	 * 
	 */
	@Override
	public JSONArray getJSONRequest() {

		JSONArray array = new JSONArray();

		JSONObject identification = new JSONObject();

		try {

			identification.put("type",Constants.Integers.TYPE_GENERAL);
			identification.put("ID", this.ID);
			identification.put("name", this.name);

			array.put(identification);

			JSONArray payload = new JSONArray();


			if(this.others != null)
				for(Timestamp timestamp: this.others.keySet())
				{

					JSONObject payloadItem = new JSONObject();
					payloadItem.put("timestamp",timestamp.toString());
					payloadItem.put("payload",this.others.get(timestamp));

					payload.put(payloadItem);
				}
			else{

				JSONObject payloadItem = new JSONObject();
				payloadItem.put("timestamp",this.timestamp);
				payloadItem.put("payload",this.payload);
				payload.put(payloadItem);

			}
			array.put(payload);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return array;
	}

}
