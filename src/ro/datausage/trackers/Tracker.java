package ro.datausage.trackers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.datausage.constants.Constants;

public abstract class Tracker {

	public String name = null;
	public int ID = -1;
	
	public abstract String getName();
	public abstract int getID();
	public abstract void updateTracker(Tracker newTracker);
	public abstract boolean equals(Tracker tracker);
	
	/**
	 * General format for the JSONArray is:
	 *  (1) - Identifier : JSONObject that contains {"type":"", "name":"", "id":""}
	 *  (2) - Data : can be anything and depends on the type of tracked information
	 * 
	 * @return
	 * @throws JSONException
	 */
	public abstract JSONArray getJSONRequest() throws JSONException;
	
	public JSONObject getIdentification()
	{
		JSONObject identification = new JSONObject();
		
		try {
			
			identification.put("name", this.name);
			identification.put("ID", this.ID);
			
			if(this.getClass().getName().contains("ActivityTracker"))
				identification.put("type", Constants.Integers.TYPE_ACTIVITY_INFO);
			else if(this.getClass().getName().contains("ButtonTracker"))
				identification.put("type", Constants.Integers.TYPE_BUTTON_INFO);
			else if(this.getClass().getName().contains("GeneralTracker"))
				identification.put("type", Constants.Integers.TYPE_GENERAL);
			else if(this.getClass().getName().contains("OnClickTracker"))
				identification.put("type", Constants.Integers.TYPE_GENERAL_CLICK);
				
			
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		
		
		
		return identification;
	}
	
	
}
