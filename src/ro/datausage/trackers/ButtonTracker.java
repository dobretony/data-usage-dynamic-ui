package ro.datausage.trackers;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.datausage.constants.Constants;
import ro.datausage.utils.Timestamp;

public class ButtonTracker extends Tracker {

	private long buttonPresses;
	private ConcurrentLinkedQueue<Timestamp> timestamps;
	private Timestamp timestamp;
	
	public ButtonTracker(String name)
	{
		this.name = name;
		this.ID = -1;
		buttonPresses = 1;
		this.timestamp = new Timestamp();
	}
	
	public ButtonTracker(int id)
	{
		this.name = null;
		this.ID = id;
		this.timestamp = new Timestamp();
		buttonPresses = 1;
	}
	
	public ButtonTracker(int id, String name)
	{
		this.name = name;
		this.ID = id;
		this.timestamp = new Timestamp();
		buttonPresses = 1;
	}
	
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getID() {
		return ID;
	}

	@Override
	public void updateTracker(Tracker newTracker) {

		if(timestamps == null){
			timestamps = new ConcurrentLinkedQueue<Timestamp>();
			timestamps.add(this.timestamp);
		}
		
		timestamps.add(((ButtonTracker)newTracker).timestamp);
		
		buttonPresses++;
	}

	@Override
	public boolean equals(Tracker tracker) {
		
		if(tracker.getClass().equals(this.getClass()))
		{
			ButtonTracker bTracker = (ButtonTracker)tracker;
			
			if(this.name != null && bTracker.getName() != null){
				if(this.name.equals(bTracker.getName()))
					return true;
			}else{
				if(this.ID == tracker.getID())
					return true;
			}
		}	
		return false;
	}
	
	/**
	 * General format for the JSONArray is:
	 *  (1) - Identifier : JSONObject that contains {"type":"TYPE_BUTTON_INFO", "name":"", "id":""}
	 *  (2) - Presses : JSONObject that contains an integer with the number of times the button was pressed this request
	 *  (3) - Timestamps : JSONArray that contains a list of timestamps for the button was pressed
	 * 
	 * @return
	 * @throws JSONException
	 */
	@Override
	public JSONArray getJSONRequest() throws JSONException {

		JSONArray retValue = new JSONArray();
		
		JSONObject buttonName = new JSONObject();
		buttonName.put("type", Constants.Integers.TYPE_BUTTON_INFO);
		buttonName.put("name", this.name);
		buttonName.put("ID", this.ID);
		
		JSONObject buttonPresses = new JSONObject();
		buttonPresses.put("presses", this.buttonPresses);
		
		JSONArray timestamps = new JSONArray();
		
		if(this.timestamps != null)
			for(Timestamp t : this.timestamps)
			{
				timestamps.put(t.toString());
			}
		else{
			timestamps.put(this.timestamp.toString());
		}
		retValue.put(buttonName);
		retValue.put(buttonPresses);
		retValue.put(timestamps);
		
		return retValue;
	}

	
	
	
}
