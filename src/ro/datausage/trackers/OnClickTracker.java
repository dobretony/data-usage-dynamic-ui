package ro.datausage.trackers;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.datausage.utils.Point;
import ro.datausage.utils.Timestamp;

public class OnClickTracker extends GeneralTracker {

	private double posX;
	private double posY;
	private String info;
	private Timestamp timestamp;

	private LinkedList<Timestamp> timestamps;
	private LinkedList<Point> arrayOfPoints;

	public OnClickTracker(String name, int id, double posX, double posY, String info){

		this.name = name;
		this.ID = id;
		this.posX = posX;
		this.posY = posY;
		this.timestamp = new Timestamp();

		arrayOfPoints = null;
		timestamps = null;
	}

	public OnClickTracker(String name, int id, double x, double y, String string,
			Timestamp timestamp) {


		this.name = name;
		this.ID = id;
		this.posX = x;
		this.posY = y;
		this.timestamp = timestamp;

		arrayOfPoints = null;
		timestamps = null;


	}

	@Override
	public void updateTracker(Tracker newTracker) {

		OnClickTracker tracker = (OnClickTracker)newTracker;

		if(arrayOfPoints == null){
			arrayOfPoints = new LinkedList<Point>();
			this.arrayOfPoints.add(new Point(posX, posY));
			this.timestamps = new LinkedList<Timestamp>();
			this.timestamps.add(this.timestamp);
		}

		if(tracker.info != null && !tracker.info.equals(""))
		{
			this.info += ";" + tracker.info;
		}

		arrayOfPoints.add(new Point(tracker.posX, tracker.posY));
		this.timestamps.add(tracker.timestamp);
	}


	@Override
	public boolean equals(Tracker tracker) {

		if(name != null){
			if(this.ID == tracker.getID())
				return true;
		}else
			if(this.name.equals(tracker.getName()))
				return true;

		return false;
	}

	/**
	 * Formatul pentru JSONRequest:s
	 * 
	 *  1- identification: a JSONObject containing 
	 *  						- name:
	 *  						- ID:
	 *  						- type: "TYPE_GENERAL_CLICK"
	 * 
	 * 	2 - the clicks: a JSON Array containg JSONObjects with:
	 * 			- posX
	 * 			- posY
	 * 			- timestamp
	 * 
	 */
	@Override
	public JSONArray getJSONRequest() {

		JSONArray array = new JSONArray();

		JSONObject identification = this.getIdentification();

		try{
			JSONArray payload = new JSONArray();

			int i = 0;

			//Point[] points = (Point[])(this.arrayOfPoints.toArray());
			//Timestamp[] timestamps = (Timestamp[])(this.timestamps.toArray());

			for(i = 0; i < this.arrayOfPoints.size(); i++)
			{
				JSONObject payloadItem = new JSONObject();

				payloadItem.put("posX", this.arrayOfPoints.get(i).x);
				payloadItem.put("posY", this.arrayOfPoints.get(i).y);
				payloadItem.put("timestamp", timestamps.get(i));

				payload.put(payloadItem);
			}
			
			array.put(identification);
			array.put(payload);
			
		}catch(JSONException e)
		{
			e.printStackTrace();
		}


		return array;
	}

}
