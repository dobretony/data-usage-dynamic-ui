package ro.datausage.trackers;

import java.util.HashMap;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.datausage.constants.Constants;
import ro.datausage.utils.Timestamp;

public class ActivityTracker extends GeneralTracker {

	public static int ACTIVITY_START = 0;
	public static int ACTIVITY_CREATE = 1;
	public static int ACTIVITY_RESUME = 2;
	public static int ACTIVITY_PAUSE = 3;
	public static int ACTIVITY_STOP = 4;
	public static int ACTIVITY_DESTROY = 5;

	private HashMap<Timestamp, Integer> eventTypes;
	private HashMap<Timestamp, String> additionalInfos;
	private Timestamp timestamp;
	private int type;
	private String additionalInfo;

	public ActivityTracker(String activityName, int id, int type) {

		if( type < 0 || type > 5){
			return;//throw an exception
		}

		this.name = activityName;
		this.ID = id;
		this.timestamp = new Timestamp();
		this.type = type;
		this.eventTypes = null;
		this.additionalInfo = null;

	}



	public ActivityTracker(String activityName, int id, int type, String additionalInfo) {

		if( type < 0 || type > 5){
			return;//throw an exception
		}

		this.name = activityName;
		this.ID = id;
		this.timestamp = new Timestamp();
		this.type = type;
		this.eventTypes = null;
		this.additionalInfo = null;
	}

	public ActivityTracker(String activityName, int type){
		if( type < 0 || type > 5){	
			return;//throw an exception
		}


		this.name = activityName;
		this.ID = -1;
		this.type = type;
		this.eventTypes = null;
		this.additionalInfo = null;
	}

	public ActivityTracker(int id, int type)
	{

		this.name = null;
		this.ID = id;
		this.type = type;

		this.eventTypes = null;
		this.additionalInfo = null;
	}

	@Override
	public void updateTracker(Tracker newTracker) {

		ActivityTracker actTracker = (ActivityTracker)newTracker;


		if(this.eventTypes == null)
		{
			this.eventTypes = new HashMap<Timestamp, Integer>();
			this.eventTypes.put(this.timestamp, this.type);
		}

		if(this.additionalInfos == null)
		{
			this.additionalInfos = new HashMap<Timestamp, String>();
			if(this.additionalInfo != null) this.additionalInfos.put(this.timestamp, this.additionalInfo);
			else this.additionalInfos.put(this.timestamp, "");
		}


		this.eventTypes.put(actTracker.timestamp, actTracker.type);
		if(actTracker.additionalInfo != null) this.additionalInfos.put(actTracker.timestamp, actTracker.additionalInfo);
		else this.additionalInfos.put(actTracker.timestamp, "");

	}




	/**
	 * The general format for the Activity tracker is:
	 * A JSONArray containing:
	 * 
	 * 1 - Activity identification stats. A JSONObject with fields:
	 * 			type:"TYPE_ACTIVITY_INFO"
	 * 			ID:""
	 * 			name:""
	 * 2 - A JSONArray containing JSONObjects with fields:
	 * 			timestamp:""
	 * 			eventtype:""
	 * 			additionalinfo:""
	 * 			
	 */
	@Override
	public JSONArray getJSONRequest() {

		JSONArray array = new JSONArray();

		JSONObject actIdentification = new JSONObject();

		try {
			actIdentification.put("type",Constants.Integers.TYPE_ACTIVITY_INFO);
			actIdentification.put("ID", this.ID);
			actIdentification.put("name", this.name);

			array.put(actIdentification);

			JSONArray payload = new JSONArray();
			if(eventTypes != null)
				for(Timestamp timestamp : eventTypes.keySet())
				{
					JSONObject payloadItem = new JSONObject();

					payloadItem.put("timestamp", timestamp.toString());
					payloadItem.put("eventtype",eventTypes.get(timestamp));
					payloadItem.put("additionalinfo", additionalInfos.get(timestamp));

					payload.put(payloadItem);

				}
			else{

				JSONObject payloadItem = new JSONObject();

				payloadItem.put("timestamp", this.timestamp);
				payloadItem.put("eventtype", this.type);
				payloadItem.put("additionalinfo", this.additionalInfo);

				payload.put(payloadItem);

			}

			array.put(payload);

		} catch (JSONException e) {
			//e.printStackTrace();
		}

		return array;
	}




	@Override
	public boolean equals(Tracker tracker) {
		return super.equals(tracker);
	}





}
