package ro.datausage.service;

import ro.datausage.exceptions.UndefinedDataUsagePropertyException;
import ro.datausage.trackers.ActivityTracker;
import ro.datausage.trackers.ButtonTracker;
import ro.datausage.trackers.OnClickTracker;
import ro.datausage.utils.Timestamp;

public class DataTracker {

	public static void initialize(TrackingManagerProperties properties) throws UndefinedDataUsagePropertyException
	{
		properties.checkValidity();
		DataUsageRepository.getInstance().initRepository(properties.getContext(), properties);
	}

	public static void terminate()
	{
		//here we should flush the system
		DataUsageRepository.getInstance().mDataUsageThread.stopRunning();
		DataUsageRepository.getInstance().mServerRequest.CleanupThreading();
		//close sockets
		//save our work in system configuration
		//etc
	}

	// -------------------------------------------------------
	//Here we begin to describe the methods for the tracking per-say
	//For eligibility, each type of track will have an equivalent object in the package
	public static void trackOnClickEvent(String name, double posX, double posY)
	{
		
	}
	
	public static void trackOnClickEvent(int id, double posX, double posY)
	{
		
	}
	
	public static void trackOnButtonPressedEvent(int id, String name)
	{
		DataUsageRepository.getInstance().trackingManager.update(new ButtonTracker(id));
	}
	
	
	public static void trackActivity(int id, String name, int state, String additionalInfo)
	{
		
		ActivityTracker actTracker = new ActivityTracker(name, id, state, additionalInfo);
		
		DataUsageRepository.getInstance().trackingManager.update(actTracker);
		
	}
	
	
	public static void trackTouchEvent(int id, String name, float x, float y)
	{
		
		OnClickTracker tracker = new OnClickTracker(name, id, x, y, "");
		
		DataUsageRepository.getInstance().trackingManager.update(tracker);
		
	}
	
	public static void trackTouchEvent(int id, String name, float x, float y, String info)
	{
		
		OnClickTracker tracker = new OnClickTracker(name, id, x, y, info);
		DataUsageRepository.getInstance().trackingManager.update(tracker);
		
	}
	
	public static void trackTouchEvent(int id, String name, double x, double y, String info, Timestamp timestamp)
	{
		
		OnClickTracker tracker = new OnClickTracker(name, id, x, y, "", timestamp);
		DataUsageRepository.getInstance().trackingManager.update(tracker);
		
	}
	
}
