package ro.datausage.service;

import ro.datausage.trackers.Tracker;

public interface TrackerListener {

	public void onReceiveTracker(Tracker t);
	public void onReceiveUpdate(int ID, String name, Tracker t);
	
}
