package ro.datausage.service;

import ro.datausage.communications.ServerRequest;

/**
 * 
 * @author Ioan Alexandru Achim
 *
 */
public class ThreadManager {
	private ServerRequest request_manager;
	private DataUsageThread dataThread;
	
	public ThreadManager() {
		request_manager = new ServerRequest();
		//dataThread = new DataUsageThread();
	}
	
	public void startMainThreads()
	{
		dataThread.startRunning();
	}
	
}
