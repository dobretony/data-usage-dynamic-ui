package ro.datausage.service;

import ro.datausage.communications.CredentialManager;
import ro.datausage.communications.ServerRequest;
import ro.datausage.database.DataBaseAsyncTask;
import ro.datausage.database.DataUsageSQLiteHelper;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;

public class DataUsageRepository {

	private static DataUsageRepository instance = null;
	private Context mContext;
	private CredentialManager mCredentialManager;
	public ServerRequest mServerRequest;
	public TrackingManager trackingManager;
	public DataUsageThread mDataUsageThread;
	private DataUsageSQLiteHelper dbHelper;
	
	private String mBuildVersion = "1";
	private String mVersionName = "1";
	private String mAppName = "";
	private String appID = "";
	
	
	/**
	 * This is a bit tricky.
	 * We have to generate this at first use in order to know an app user is correctly identified.
	 * Future implementations will allow secure connections and identifications per user.
	 * At the moment we will just log as anonymous user. ( no identification data for the statistics sent )
	 */
	private String deviceUID = "";
	
	
	private DataUsageRepository()
	{
		mContext = null;
	}
	
	public static DataUsageRepository getInstance()
	{
		if(instance == null)
			instance = new DataUsageRepository();
		
		return instance;
	}
	
	public void initRepository(Context context, TrackingManagerProperties properties)
	{
		this.mContext = context;
		
		//first we setup our internet connection
		
		//then we setup our credential manager 
		mCredentialManager = new CredentialManager();
		mCredentialManager.registerAppId(properties.getAppID());
		mCredentialManager.registerStartTime();
		
		//afterwards we instantiate the server request class - curtousy of Achim 
		mServerRequest = new ServerRequest();
		mServerRequest.InitializeThreading();
		mServerRequest.SetWebrequestServer("http://tracingtest2-iachim.rhcloud.com/api/receive");
		mServerRequest.SetCredentialManager(mCredentialManager);

		this.appID = properties.getAppID();
		
		//create the helper here
		this.dbHelper = new DataUsageSQLiteHelper(context);
		
		//we instantiate our thread for connection to the server
		mDataUsageThread = new DataUsageThread(this.dbHelper);
		
		//we instantiate our tracker manager
		trackingManager = new TrackingManager();
		
		//adding listeners to our tracking manager
		trackingManager.addListener(mDataUsageThread);
		
		String versionName = "";
		String buildVersion = "";
		String appName = "ro.datausage";
		try {
			versionName = context.getPackageManager()
				    .getPackageInfo(context.getPackageName(), 0).versionName;
			
			buildVersion += context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
			
			int stringId = context.getApplicationInfo().labelRes;
			appName = context.getString(stringId);
			
		} catch (NameNotFoundException e) {
			versionName = "1";
			buildVersion = "1";
		} catch (Exception e){
			//oops i did it again
		}
		
		mBuildVersion = buildVersion;
		mVersionName = versionName;
		this.mAppName = appName;
		
		
		
		
		try{
			
			mDataUsageThread.openDataBase();
			mDataUsageThread.createNewAppInstance();
			
		}catch(SQLException e){
			
			///here we should set some flags in order to not use data bases
			
		}
		
		new DataBaseAsyncTask(this.dbHelper).execute();
		
		//finnaly we launch the main thread
		mDataUsageThread.startRunning();
		
	}

	public Context getServiceContext() {
		return mContext;
	}
	
	
	public void saveSettings()
	{
		
	}
	
	public void restoreSettings()
	{
		
	}

	public String getBuildVersion() {
		return mBuildVersion;
	}

	public String getVersionName() {
		return mVersionName;
	}

	public String getAppName() {
		return mAppName;
	}

	public String getAppID() {
		return appID;
	}

	
}
