package ro.datausage.service;

import java.util.concurrent.ConcurrentLinkedQueue;

import ro.datausage.trackers.Tracker;
import android.util.Log;

public class TrackingManager {

	private ConcurrentLinkedQueue<TrackerListener> listeners;
	private int lastID;

	public TrackingManager()
	{
		listeners = new ConcurrentLinkedQueue<TrackerListener>();
		lastID = -1;
	}

	
	public void update(Tracker tracker)
	{
		this.notifyListeners(tracker);
	}

	public int returnLastID()
	{
		return lastID;
	}

	public void addListener(TrackerListener tl)
	{
		listeners.add(tl);
	}

	public void notifyListeners(Tracker tracker)
	{
		for(TrackerListener tl : listeners)
		{
			tl.onReceiveTracker(tracker);	
		}
		
	}
	
}
