package ro.datausage.service;

import java.util.ArrayList;

import ro.datausage.exceptions.UndefinedDataUsagePropertyException;

import android.content.Context;

public class TrackingManagerProperties {

	//TODO: make a final holder for data
		private String appID;
		private String appName;
		private String userID;
		private String userEmail;
		private String userPass;
		private boolean onlineConnection;
		private String appDescription;
		private String serverIP;
		private String serverPort;
		private String nameOfLocalRepository;
		private Context initialContext;
		
		public TrackingManagerProperties()
		{
			appID = null;
			appName = null;
			onlineConnection = false;
			appDescription = "";
			serverIP = null;
			serverPort = null;
			nameOfLocalRepository = null;
			initialContext = null;
			userID = null;
			userEmail = null;
			userPass = null;
		}
		
		/**
		 * Used in initialize API in order to see if any essential properties are left out.
		 * Essential properties are:
		 *    - appID
		 *    - appName
		 *    - serverIP
		 *    - serverPort
		 *    - mContext
		 * @return
		 * @throws UndefinedDataUsagePropertyException 
		 */
		public void checkValidity() throws UndefinedDataUsagePropertyException
		{
			if(appID == null){
				throw new UndefinedDataUsagePropertyException("Application ID (appID)");
			}
			
			if(appName == null){
				throw new UndefinedDataUsagePropertyException("Application Name (appName)");
			}
			
			if(serverIP == null){
				throw new UndefinedDataUsagePropertyException("Cloud Server IP (serverIP)");
			}
			
			if(serverPort == null){
				throw new UndefinedDataUsagePropertyException("Cloud Server Port (serverPort)");
			}
			
			if(initialContext == null){
				throw new UndefinedDataUsagePropertyException("Initial Context (initialContext)");
			}
			
		}
		
		public void setID(String ID){
			
			this.appID = ID;
			
		}
		
		public void setName(String name)
		{
			this.appName = name;
		}
				
		
		public void setDescription(String description)
		{
			this.setAppDescription(description);
		}
		
		public void setOnlineConnectivity(boolean value)
		{
			this.setOnlineConnection(value);
		}

		public void setUID(String uid)
		{
			this.userID = uid;
		}
		
		public void setPassword(String pass)
		{
			this.userPass = pass;
		}
		
		public void setUserEmail(String email)
		{
			this.userEmail = email;
		}
		
		public String getUID()
		{
			return this.userID;
		}
		
		public String getPassword()
		{
			return this.userPass;
		}
		
		public String getEmail()
		{
			return this.userEmail;
		}
		
		public Context getContext() {
			return initialContext;
		}

		public void setContext(Context mContext) {
			this.initialContext = mContext;
		}

		public String getAppID() {
			return appID;
		}

		public String getName() {
			return appName;
		}

		public String getNameOfLocalRepository() {
			return nameOfLocalRepository;
		}

		public void setNameOfLocalRepository(String nameOfLocalRepository) {
			this.nameOfLocalRepository = nameOfLocalRepository;
		}

		public String getServerIP() {
			return serverIP;
		}

		public void setServerIP(String serverIP) {
			this.serverIP = serverIP;
		}

		public String getServerPort() {
			return serverPort;
		}

		public void setServerPort(String serverPort) {
			this.serverPort = serverPort;
		}

		public String getAppDescription() {
			return appDescription;
		}

		public void setAppDescription(String appDescription) {
			this.appDescription = appDescription;
		}

		public boolean isOnlineConnection() {
			return onlineConnection;
		}

		public void setOnlineConnection(boolean onlineConnection) {
			this.onlineConnection = onlineConnection;
		}

	
	
}
