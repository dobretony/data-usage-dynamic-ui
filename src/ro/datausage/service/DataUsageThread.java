package ro.datausage.service;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.json.JSONException;

import ro.datausage.communications.ServerRequest;
import ro.datausage.constants.Constants;
import ro.datausage.database.DataUsageSQLiteHelper;
import ro.datausage.trackers.Tracker;
import ro.datausage.utils.Timestamp;
import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DataUsageThread implements Runnable, TrackerListener {

	private boolean run;
	private DataUsageThreadHandler mHandler;
	private ServerRequest mServerRequest;
	private ConcurrentLinkedQueue<Tracker> trackers;
	private DataUsageSQLiteHelper helper;
	private String[] app_InstanceColumns = {"_id", "timestamp_begin", "timestamp_end", "app_version", "app_name"};
	private String[] app_eventsColumns = {"_id", "data", "app_instance_id"};
	private DataUsageRepository repository;
	/**
	 * The id where all the insert in the app_events table should go. 
	 */
	private long insertId;


	public DataUsageThread( DataUsageSQLiteHelper databaseHelper )
	{

		//instantiate object variables
		run = false;
		repository = DataUsageRepository.getInstance();
		//this is the server request object. this should be removed in future versions
		mServerRequest = DataUsageRepository.getInstance().mServerRequest;
		//the list of trackers
		trackers = new ConcurrentLinkedQueue<Tracker>();
		//the sql lite database helper
		this.helper = databaseHelper;		

	}


	public void openDataBase() throws SQLException
	{

		SQLiteDatabase database;
		database = helper.getWritableDatabase();
		database.acquireReference();
	}

	public void createNewAppInstance()
	{
		synchronized (helper) {


			SQLiteDatabase database;

			database = helper.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(app_InstanceColumns[1], new Timestamp().toString());
			values.put(app_InstanceColumns[2], "end");
			values.put(app_InstanceColumns[3], repository.getBuildVersion());
			values.put(app_InstanceColumns[4], repository.getAppName());

			long insertId = database.insert(Constants.Strings.TABLE_APP_INSTANCE_NAME, null, values);

			this.insertId = insertId;

			database.close();

		}
	}



	@Override
	public void run() {

		while(run)
		{
			try {

				Thread.sleep(Constants.Integers.THREAD_RUNNING_TIME);//we can change this dynamically.. yey
				//sendToWebServer();
				saveToDataBase();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}


	}

	public boolean isRunning()
	{
		return run;
	}


	public void startRunning() {

		run = true;
		new Thread(this).start();

	}



	public void stopRunning()
	{

		run = false;
		this.saveToDataBase();
		this.finishInstance();
		this.helper.close();
	}


	private void finishInstance()
	{

		SQLiteDatabase database;

		database = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(app_InstanceColumns[2], new Timestamp().toString());
		database.update(Constants.Strings.TABLE_APP_INSTANCE_NAME, values, "_id=" + insertId, null);

		database.close();
	}


	public DataUsageThreadHandler getHandler() {
		return mHandler;
	}


	public void setHandler(DataUsageThreadHandler mHandler) {
		this.mHandler = mHandler;
	}


	@Override
	public void onReceiveTracker(Tracker t) {

		boolean updated = false;
		synchronized (trackers) {


			for(Tracker tr : trackers)
			{
				if(tr.equals(t)){
					tr.updateTracker(t);
					updated = true;
				}
			}

			if(updated == false)
				trackers.add(t);
		}
	}


	@Override
	public void onReceiveUpdate(int ID, String name, Tracker t) {


	}

	public void sendToWebServer()
	{
		if(trackers == null || trackers.isEmpty())
			return;

		synchronized (trackers) {


			for(Tracker t : trackers)
			{
				try {

					mServerRequest.AddRequestToList(t.getJSONRequest());

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			mServerRequest.flush();


			this.trackers.clear();
		}
	}


	public void saveToDataBase()
	{

		synchronized(helper){

			SQLiteDatabase database;
			database = helper.getWritableDatabase();

			//return if another thread (like the send to server async task ) 
			if(database.isDbLockedByOtherThreads() || database.isDbLockedByCurrentThread()){
				return;
			}

			ConcurrentLinkedQueue<Tracker> auxList = null;

			synchronized (trackers) {
				auxList = new ConcurrentLinkedQueue<Tracker>(trackers);
				trackers.clear();
			}

			try{
				ContentValues values  = null;
				for(Tracker t : auxList)
				{

					values = new ContentValues();
					values.put(app_eventsColumns[1], t.getJSONRequest().toString());
					values.put(app_eventsColumns[2], this.insertId);

					database.insert(Constants.Strings.TABLE_APP_EVENTS_NAME, null, values);

				}

			}catch (JSONException e) {

				//the event was lost.
				Log.d("test", "JSONException in DataUsageThread.saveToDataBase(): " + e.getMessage());


			}catch (SQLException e){

				//the event was lost

				Log.d("test","SQLException in DataUsageThread.saveToDataBase(): " + e.getMessage());

			}finally{
				database.close();
			}

		}
	}


	public void printDebug()
	{
		Log.d("Test","Starting thread dump.");
		try{

			for(Tracker t : trackers)
			{
				Log.d("Test", t.getJSONRequest().toString());
			}

		}catch(JSONException e)
		{
			Log.d("Test","JSON Exception : " + e.getLocalizedMessage());
		}
		Log.d("Test", "End of thread dump.");
	}

	public void printDebug(Tracker t)
	{
		Log.d("Test", "Starting tracker dump.");


		try {

			Log.d("Test", t.getJSONRequest().toString(1));

		} catch (JSONException e) {
			e.printStackTrace();
		}


		Log.d("Test", "End tracker dump.");
	}

}
