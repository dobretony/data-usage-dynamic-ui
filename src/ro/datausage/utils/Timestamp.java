package ro.datausage.utils;

import java.util.Calendar;

public class Timestamp {

	private int millisecond;
	private int second;
	private int minute;
	private int hour;
	private int day;
	private int month;
	private int year;
	
	public Timestamp()
	{
		Calendar calendar = Calendar.getInstance();
		
		this.millisecond = calendar.get(Calendar.MILLISECOND);
		this.second = calendar.get(Calendar.SECOND);
		this.minute = calendar.get(Calendar.MINUTE);
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.day = calendar.get(Calendar.DAY_OF_MONTH);
		this.month = calendar.get(Calendar.MONTH);
		this.year = calendar.get(Calendar.YEAR);
		
	}
	
	
	@Override
	public String toString()
	{
		return "" + this.year + "-" + this.month + 
				"-" + this.day + 
				" " + this.hour + ":" + this.minute +
				":" + this.second + ":" + this.millisecond;
	}
	
	
}
