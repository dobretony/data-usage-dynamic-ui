package ro.datausage.utils;

import java.util.Calendar;

public class Point {

	public double x;
	public double y;
	public String timestamp;
	
	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
		
		Calendar c = Calendar.getInstance();
		this.timestamp = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH) + 
				"-" + c.get(Calendar.DAY_OF_WEEK_IN_MONTH) + 
				" :" + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) +
				":" + c.get(Calendar.SECOND) + ":" + c.get(Calendar.MILLISECOND);
		
	}
	
}
