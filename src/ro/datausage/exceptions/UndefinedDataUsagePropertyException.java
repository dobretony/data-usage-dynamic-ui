package ro.datausage.exceptions;

public class UndefinedDataUsagePropertyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5744418049025700331L;

	public UndefinedDataUsagePropertyException(String nameOfProperty)
	{
		super("Please fill in the " + nameOfProperty + " property when you are trying to initialize this API." );
	}
	
	public UndefinedDataUsagePropertyException()
	{
		
	}
}
